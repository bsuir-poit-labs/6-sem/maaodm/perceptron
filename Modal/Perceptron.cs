﻿using System.Text;
using System.Windows.Forms;

namespace Perceptron.Modal
{
    public class Perceptron
    {
        private const int MaxIterationsCount = 10_000;

        private readonly PerceptronClass[] _classes;
        private readonly PerceptronObject[] _weights;
        private readonly int[] _decisions;
        private bool _isMaxIterated;

        public Perceptron(int classesCount, int objectsCount, int attributesCount)
        {
            _classes = new PerceptronClass[classesCount];
            _weights = new PerceptronObject[classesCount];
            _decisions = new int[classesCount];

            for (var i = 0; i < classesCount; i++)
            {
                _classes[i] = new PerceptronClass(objectsCount, attributesCount);
                _weights[i] = new PerceptronObject(attributesCount, 0);
            }
        }

        public void Calculate()
        {
            bool isClassifiedCorrectly = false;
            int iteration = 0;

            do
            {
                for (var i = 0; i < _classes.Length; i++)
                {
                    var perceptronClass = _classes[i];
                    var weight = _weights[i];

                    foreach (var perceptronObject in perceptronClass.Objects)
                    {
                        isClassifiedCorrectly = CorrectWeight(perceptronObject, weight, i);
                    }
                }

                iteration++;
            } while (iteration < MaxIterationsCount && !isClassifiedCorrectly);

            _isMaxIterated = iteration == MaxIterationsCount;
        }

        private bool CorrectWeight(PerceptronObject perceptronObject, PerceptronObject weight, int classNumber)
        {
            var result = true;
            int objectDecision = ObjectMultiplication(weight, perceptronObject);

            for (var i = 0; i < _weights.Length; i++)
            {
                _decisions[i] = ObjectMultiplication(_weights[i], perceptronObject);

                if (i == classNumber)
                    continue;

                var currentDecision = _decisions[i];
                if (objectDecision <= currentDecision)
                {
                    ChangeWeight(_weights[i], perceptronObject, -1);
                    result = false;
                }
            }

            if (result)
                ChangeWeight(weight, perceptronObject, 1);

            return result;
        }

        private static int ObjectMultiplication(PerceptronObject obj1, PerceptronObject obj2)
        {
            var sum = 0;
            for (var i = 0; i < obj1.Attributes.Length; i++)
            {
                sum += obj1.Attributes[i] * obj2.Attributes[i];
            }

            return sum;
        }

        private static void ChangeWeight(PerceptronObject weight, PerceptronObject perceptronObject, int sign)
        {
            for (var i = 0; i < weight.Attributes.Length; i++)
                weight.Attributes[i] += sign * perceptronObject.Attributes[i];
        }

        public void FillListBox(TextBox textBox)
        {
            textBox.Clear();
            var stringBuilder = new StringBuilder();

            if (_isMaxIterated)
            {
                stringBuilder.AppendLine($"Количество итераций превысило {MaxIterationsCount}. ");
                stringBuilder.AppendLine("Решаюшие функции, возможно, найдены неправильно.");
                stringBuilder.AppendLine();
            }

            stringBuilder.AppendLine(ClassesToString());
            stringBuilder.AppendLine(PrintDecisionFunctions());

            textBox.Text = stringBuilder.ToString();
        }

        private string ClassesToString()
        {
            var stringBuilder = new StringBuilder();

            for (var i = 0; i < _classes.Length; i++)
            {
                stringBuilder.AppendLine($"Класс {i + 1}:");
                stringBuilder.Append(_classes[i]);
                stringBuilder.AppendLine();
            }

            return stringBuilder.ToString();
        }

        private string PrintDecisionFunctions()
        {
            var stringBuilder = new StringBuilder("Решающие функции:\r\n");

            for (var i = 0; i < _weights.Length; i++)
            {
                stringBuilder.Append($"d{i + 1}(x) = ");

                for (var j = 0; j < _weights[i].Attributes.Length; j++)
                {
                    var attribute = _weights[i].Attributes[j];

                    if (j < _weights[i].Attributes.Length - 1)
                        if (attribute >= 0 && j != 0)
                            stringBuilder.Append($"+{attribute}*x{j + 1}");
                        else
                            stringBuilder.Append($"{attribute}*x{j + 1}");
                    else if (attribute >= 0 && j != 0)
                        stringBuilder.Append($"+{attribute}");
                    else
                        stringBuilder.Append(attribute);
                }

                stringBuilder.AppendLine();
            }

            return stringBuilder.ToString();
        }

        public int FindClass(PerceptronObject obj)
        {
            var maxClass = 0;
            var maxDecision = ObjectMultiplication(_weights[0], obj);

            for (var i = 1; i < _weights.Length; i++)
            {
                var decision = ObjectMultiplication(_weights[i], obj);
                if (decision > maxDecision)
                {
                    maxDecision = decision;
                    maxClass = i;
                }
            }

            return maxClass + 1;
        }
    }
}