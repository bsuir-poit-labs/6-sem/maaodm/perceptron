﻿using System;
using System.Text;

namespace Perceptron.Modal
{
    public class PerceptronObject
    {
        private static readonly Random Random = new();
        private const int MaxValue = 10;
        private const int C = 1;

        public int[] Attributes { get; }

        public PerceptronObject(int attributesCount)
        {
            Attributes = new int[attributesCount + 1];
            for (var i = 0; i < attributesCount; i++)
            {
                Attributes[i] = Random.Next(MaxValue) - MaxValue / 2;
            }

            Attributes[attributesCount] = C;
        }

        public PerceptronObject(int attributesCount, int value)
        {
            Attributes = new int[attributesCount + 1];
            for (var i = 0; i < Attributes.Length; i++)
            {
                Attributes[i] = value;
            }
        }

        public PerceptronObject(int[] attributes)
        {
            Attributes = new int[attributes.Length + 1];

            for (var i = 0; i < attributes.Length; i++)
            {
                Attributes[i] = attributes[i];
            }

            Attributes[attributes.Length] = C;
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder("(");

            for (var i = 0; i < Attributes.Length - 1; i++)
            {
                stringBuilder.Append($"{Attributes[i]}, ");
            }

            stringBuilder.Remove(stringBuilder.Length - 2, 2);
            stringBuilder.Append(')');

            return stringBuilder.ToString();
        }
    }
}