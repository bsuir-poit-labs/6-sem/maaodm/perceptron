﻿using System.Text;

namespace Perceptron.Modal
{
    public class PerceptronClass
    {
        public PerceptronObject[] Objects { get; }

        public PerceptronClass(int objectsCount, int attributesCount)
        {
            Objects = new PerceptronObject[objectsCount];
            for (var i = 0; i < objectsCount; i++)
            {
                Objects[i] = new PerceptronObject(attributesCount);
            }
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            for (var i = 0; i < Objects.Length; i++)
            {
                stringBuilder.AppendLine($"    Объект {i + 1}: {Objects[i]}");
            }

            return stringBuilder.ToString();
        }
    }
}