﻿using System;
using System.Windows.Forms;
using Perceptron.Modal;

namespace Perceptron
{
    public partial class MainForm : Form
    {
        private Modal.Perceptron _perceptron;

        public MainForm()
        {
            InitializeComponent();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            var classes = (int) nudClass.Value;
            var objects = (int) nudObject.Value;
            var attributes = (int) nudAttributes.Value;

            _perceptron = new Modal.Perceptron(classes, objects, attributes);
            _perceptron.Calculate();
            _perceptron.FillListBox(textBox);

            dataGridView.RowCount = attributes;
            dataGridView.ColumnCount = 1;
        }

        private void btnFindClass_Click(object sender, EventArgs e)
        {
            var attributes = new int[dataGridView.RowCount];
            try
            {
                for (int i = 0; i < attributes.Length; i++)
                {
                    attributes[i] = int.Parse(dataGridView.Rows[i].Cells[0].Value.ToString() ??
                                              throw new InvalidOperationException());
                }

                var perceptronObject = new PerceptronObject(attributes);
                var classNumber = _perceptron.FindClass(perceptronObject);
                MessageBox.Show($@"Объект относится к {classNumber} классу.");
            }
            catch (Exception)
            {
                MessageBox.Show(@"Ошибка ввода!");
            }
        }
    }
}