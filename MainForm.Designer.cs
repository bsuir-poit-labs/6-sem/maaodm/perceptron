﻿namespace Perceptron
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel = new System.Windows.Forms.Panel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnFindClass = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.nudClass = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.nudObject = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.nudAttributes = new System.Windows.Forms.NumericUpDown();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.textBox = new System.Windows.Forms.TextBox();
            this.panel.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.dataGridView)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.nudClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudObject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudAttributes)).BeginInit();
            this.SuspendLayout();
            // 
            // panel
            // 
            this.panel.Controls.Add(this.flowLayoutPanel2);
            this.panel.Controls.Add(this.flowLayoutPanel1);
            this.panel.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel.Location = new System.Drawing.Point(0, 0);
            this.panel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(332, 659);
            this.panel.TabIndex = 0;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.btnFindClass);
            this.flowLayoutPanel2.Controls.Add(this.dataGridView);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.BottomUp;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 416);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(14);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(332, 243);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // btnFindClass
            // 
            this.btnFindClass.Location = new System.Drawing.Point(62, 194);
            this.btnFindClass.Margin = new System.Windows.Forms.Padding(62, 14, 14, 14);
            this.btnFindClass.Name = "btnFindClass";
            this.btnFindClass.Size = new System.Drawing.Size(193, 35);
            this.btnFindClass.TabIndex = 0;
            this.btnFindClass.Text = "Подобрать класс";
            this.btnFindClass.UseVisualStyleBackColor = true;
            this.btnFindClass.Click += new System.EventHandler(this.btnFindClass_Click);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(62, 24);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(62, 14, 14, 14);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.Size = new System.Drawing.Size(193, 142);
            this.dataGridView.TabIndex = 1;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AllowDrop = true;
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.nudClass);
            this.flowLayoutPanel1.Controls.Add(this.label2);
            this.flowLayoutPanel1.Controls.Add(this.nudObject);
            this.flowLayoutPanel1.Controls.Add(this.label3);
            this.flowLayoutPanel1.Controls.Add(this.nudAttributes);
            this.flowLayoutPanel1.Controls.Add(this.btnCalculate);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(14);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(332, 349);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(17, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(246, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Количество классов\r\n\r\n";
            // 
            // nudClass
            // 
            this.nudClass.Location = new System.Drawing.Point(28, 51);
            this.nudClass.Margin = new System.Windows.Forms.Padding(14);
            this.nudClass.Name = "nudClass";
            this.nudClass.Size = new System.Drawing.Size(120, 34);
            this.nudClass.TabIndex = 1;
            this.nudClass.Value = new decimal(new int[] {3, 0, 0, 0});
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(17, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(284, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Количество объектов класса";
            // 
            // nudObject
            // 
            this.nudObject.Location = new System.Drawing.Point(28, 136);
            this.nudObject.Margin = new System.Windows.Forms.Padding(14);
            this.nudObject.Name = "nudObject";
            this.nudObject.Size = new System.Drawing.Size(120, 34);
            this.nudObject.TabIndex = 3;
            this.nudObject.Value = new decimal(new int[] {2, 0, 0, 0});
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(17, 184);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(325, 30);
            this.label3.TabIndex = 4;
            this.label3.Text = "Количество признаков объекта\r\n";
            // 
            // nudAttributes
            // 
            this.nudAttributes.Location = new System.Drawing.Point(28, 228);
            this.nudAttributes.Margin = new System.Windows.Forms.Padding(14);
            this.nudAttributes.Name = "nudAttributes";
            this.nudAttributes.Size = new System.Drawing.Size(120, 34);
            this.nudAttributes.TabIndex = 5;
            this.nudAttributes.Value = new decimal(new int[] {2, 0, 0, 0});
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(17, 290);
            this.btnCalculate.Margin = new System.Windows.Forms.Padding(3, 14, 3, 3);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(300, 36);
            this.btnCalculate.TabIndex = 6;
            this.btnCalculate.Text = "Создать обучающую выборку";
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // textBox
            // 
            this.textBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox.Location = new System.Drawing.Point(332, 0);
            this.textBox.Multiline = true;
            this.textBox.Name = "textBox";
            this.textBox.ReadOnly = true;
            this.textBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox.Size = new System.Drawing.Size(587, 659);
            this.textBox.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(919, 659);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.panel);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (204)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Персептрон";
            this.panel.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.dataGridView)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.nudClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudObject)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.nudAttributes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.DataGridView dataGridView;

        private System.Windows.Forms.Button btnFindClass;

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;

        private System.Windows.Forms.TextBox textBox;

        private System.Windows.Forms.Button btnCalculate;

        private System.Windows.Forms.NumericUpDown nudAttributes;
        
        private System.Windows.Forms.NumericUpDown nudObject;
        
        private System.Windows.Forms.Label label3;

        private System.Windows.Forms.Label label2;

        private System.Windows.Forms.NumericUpDown nudClass;

        private System.Windows.Forms.Label label1;

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;

        private System.Windows.Forms.Panel panel;

        #endregion
    }
}